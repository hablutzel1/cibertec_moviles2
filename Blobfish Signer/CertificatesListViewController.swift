//
//  CertificatesListViewController.swift
//  Test1
//
//  Created by Admin on 11/12/16.
//  Copyright © 2016 blobfish. All rights reserved.
//

import UIKit
import PSPDFKit


// TODO evaluate to use the system mechanism to list certificates, see https://developer.apple.com/library/content/qa/qa1745/_index.html, it would maybe require to create a custom PSPDFSigner. instead of using the PSPDFPKCS12Signer.
class CertificateListViewController: UITableViewController {

    var signers: [PSPDFSigner] = []

    // TODO confirm: does viewDidLoad run before the actual view is displayed?.
    override func viewDidLoad() {
        // TODO check if the call to the parent function is required.
        super.viewDidLoad()
        // TODO check if there is a shortcut method to get to Inbox/.
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        // FIXME if no pkcs12 has been previously imported, the Inbox/ folder won't exist and the application will fail.
        let inboxDirectory = NSString(string: documentsDirectory).appendingPathComponent("Inbox") as NSString
        let p12Filenames = try! FileManager.default.contentsOfDirectory(atPath: inboxDirectory as String).filter({$0.hasSuffix(".pkcs12")})
        for p12Filename in p12Filenames {
            let p12FullPath = inboxDirectory.appendingPathComponent(p12Filename)
            let p12 = PSPDFPKCS12(data: NSData(contentsOfFile: p12FullPath) as! Data)
            let p12Signer = PSPDFPKCS12Signer(displayName: p12Filename, pkcs12: p12)
            signers.append(p12Signer)
        }

    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return signers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrototypeCell", for: indexPath)
        cell.textLabel?.text = signers[indexPath.row].displayName
        return cell
    }

    /// Used to pass the chosen signer to the SignViewController.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let signViewController = segue.destination as! SignViewController
        let selectedIndexPath = self.tableView.indexPath(for: sender as! UITableViewCell)
        signViewController.chosenSigner = signers[(selectedIndexPath?.row)!]
    }
 

}
