//
//  ViewController.swift
//  Blobfish Signer
//
//  Created by Admin on 11/13/16.
//  Copyright © 2016 blobfish. All rights reserved.
//

import UIKit
import PSPDFKit

class ViewController: UIViewController {

    // TODO clarify the difference between @selector(viewDidLoad) and @selector(viewDidAppear:) and check if we could use the former.
    override func viewDidAppear(_ animated: Bool) {
        if (DataSingleton.singleton.tbsFile != nil) { // If the application has been invoked from a "bsign" URI.
            let pdfDocument = PSPDFDocument(url: DataSingleton.singleton.tbsFile!)
            let pdfController = MyPSPDFViewController(document: pdfDocument, mainStoryboard: self.storyboard!)
            present(UINavigationController(rootViewController: pdfController), animated: true)
        } else if (DataSingleton.singleton.pfxIsBeingImported) { // If the application has been invoked from a .pkcs12 file.
            // NOTE that in this case we just display a success message as the .pkcs12 is automatically saved in to /Documents/Inbox.
            showAlert("Certificate imported succesfully.") {
                timer in
                // FIXME do not hardcode this callbackurl.
                let callbackURL = URL(string: "http://demo.blobfish.pe:8080/blobfish-signer-webapp-ios/")
                UIApplication.shared.openURL(callbackURL!)
            }
        } else { // If the has been opened directly from the apps menu.
            showAlert("This app only supports to be called from the browser.") {
                timer in
                exit(0)
            }

        }
    }

    func showAlert(_ message: String, block: @escaping (Timer) -> Void) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: block)
    }

}

