//
//  SignViewController.swift
//  Test1
//
//  Created by Admin on 11/12/16.
//  Copyright © 2016 blobfish. All rights reserved.
//

import UIKit
import PSPDFKit

class SignViewController: UIViewController {

    public var chosenSigner: PSPDFSigner?

    @IBOutlet weak var passwordTextField: UITextField!

    @IBAction func completeSignatureOperation(_ sender: Any) {
        // See the "Automated digital signing process" demo in the PSPDFCatalog samples project.
        let pdfDocument = PSPDFDocument(url: DataSingleton.singleton.tbsFile!)
        let annotations = pdfDocument.annotationsForPage(at: 0, type: PSPDFAnnotationType.widget)
        var element: PSPDFSignatureFormElement? = nil
        for a in annotations! {
            if (a is PSPDFSignatureFormElement) {
                // TODO clarify the difference between as? and as!
                element = a as? PSPDFSignatureFormElement;
            }
        }

        let tempDirectory = NSTemporaryDirectory() as NSString
        let tempFileName = NSString.init(format: "signed-%@.pdf", NSUUID.init().uuidString)
        let signedFile = tempDirectory.appendingPathComponent(tempFileName as String)
        // TODO try to cast inline if it is possible.
        let pkcs12Signer = chosenSigner as! PSPDFPKCS12Signer
        pkcs12Signer.sign(element!, usingPassword: passwordTextField.text!, writeTo: signedFile, completion: {
            (a: Bool, b: PSPDFDocument, c: Error?) in

            let uploadLocation = URL(string: DataSingleton.singleton.documentUploadLocation!)
            let urlRequest = NSMutableURLRequest(url: uploadLocation!)
            urlRequest.httpMethod = "POST"
            let signedFileData = NSData(contentsOfFile: signedFile)
            urlRequest.httpBody = signedFileData as Data?

            // TODO make it asynchronous and display some loading indicator during the upload.
            try! NSURLConnection.sendSynchronousRequest(urlRequest as URLRequest, returning: nil)

            // FIXME do not hardcode it.
            let callbackURL = URL(string: "http://demo.blobfish.pe:8080/blobfish-signer-webapp-ios/")
            UIApplication.shared.openURL(callbackURL!)
        })

    }

}
