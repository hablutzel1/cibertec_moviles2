//
// Created by Admin on 11/22/16.
// Copyright (c) 2016 blobfish. All rights reserved.
//

import Foundation

class DataSingleton {

    // TODO check if this is the best way to create a singleton in Swift.
    static let singleton : DataSingleton = DataSingleton()

    /// The cached file to be signed, it is stored in this variable after being downloaded from server.
    var tbsFile: URL?

    /// The location where the signed document should be uploaded.
    var documentUploadLocation: String?

    /// Special variable to indicate that the application has been started to import a PKCS #12 or PFX file.
    var pfxIsBeingImported: Bool = false

}
