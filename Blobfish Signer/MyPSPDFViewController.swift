//
// Created by Admin on 11/12/16.
// Copyright (c) 2016 blobfish. All rights reserved.
//

import Foundation
import PSPDFKit

/// Custom PSPDFViewController which adds a "Sign" bar button item.
class MyPSPDFViewController: PSPDFViewController {

    // TODO try to make mainStoryboard non-optional, this will maybe require to have only one init method.
    // TODO check if this is the right method to be able to use views constructed in Main.storyboard visual editor.
    /// A reference to the main storyboard passed from the default ViewController.
    var mainStoryboard: UIStoryboard?

    public init(document: PSPDFDocument, mainStoryboard: UIStoryboard) {
        super.init(document: document, configuration: nil)
        self.mainStoryboard = mainStoryboard
    }

    // TODO check why is it required to implement this init method.
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        // TODO try to put this button in the right.
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Sign", style: UIBarButtonItemStyle.plain, target: nil, action: #selector(MyPSPDFViewController.openCertificateList))
    }

    func openCertificateList() {
        let certificateListViewController = mainStoryboard?.instantiateViewController(withIdentifier: "CertificateListViewController")
        // TODO research on UINavigationController and the animated parameter for present.
        present(UINavigationController(rootViewController: certificateListViewController!), animated: true)
    }
}
