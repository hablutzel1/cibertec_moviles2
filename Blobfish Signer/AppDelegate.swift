//
//  AppDelegate.swift
//  Blobfish Signer
//
//  Created by Admin on 11/13/16.
//  Copyright © 2016 blobfish. All rights reserved.
//

import UIKit
import PSPDFKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [:]) -> Bool {

        // First the PSPDFKit license is installed, but NOTE that it is only required when an operation with PDFs is to be done.
        PSPDFKit.setLicenseKey("qdK4qv6LjRRnMo8iRLJ4wcl9hexQJC1AiHNH16VpdAlWMdOR4Le9TD8VypT4CK8wyrsg0G5QOVsajzhJy30JFMjLPowzkyPuSNDbJRoK6UTG1K62ueITpb2bKVmhBkEkb/gXJsGzilGSYophg+7HlnSw/O1aElfF/rvpUxRCBlh/7bKnUnZgPdfilZMxOpPsvx81ImfcyKLWLqJ+kUm1wyihx3wOuIdWEHFMg0B14I10GTIKCbLd5OOf8oobYQZX86CGWSaodPLdvLCbgvzrjTolCkyQ/kdqCruioClRwkOWG1IqrhqK/f5qpxOxozz/hSWot66t9gMODgGCAbMruOaIGqB102opGodVzfMnRDCAvE3LrGMynrRzfQi3jsJUQrxB5KWbPooAnspa0qG2hg09HH7f/ry0rMWVV4/D2tNZrN3SbOaCQzJWZVHk4Sg0")

        let urlComponents = NSURLComponents(string: url.absoluteString)
        if (urlComponents?.queryItems != nil) {
            // This is a "bsign" URI.
            // Create a temp file.
            let tempDirectory = NSTemporaryDirectory() as NSString
            let tempFileName = NSString(format: "unsigned-%@.pdf", NSUUID.init().uuidString)
            DataSingleton.singleton.tbsFile = NSURL(fileURLWithPath: tempDirectory.appendingPathComponent(tempFileName as String)) as URL

            // Download the document to the temp file.
            let documentDownloadLocation = URL(string: extractURIParameter(url.absoluteString, name: "from"))
            // TODO make the download operation asynchronous.
            let documentNSData = NSData(contentsOf: documentDownloadLocation!)
            documentNSData?.write(to: DataSingleton.singleton.tbsFile!, atomically: true)

            //  save the 'to' reference.
            DataSingleton.singleton.documentUploadLocation = extractURIParameter(url.absoluteString, name: "to")

            return true;
        } else {
            DataSingleton.singleton.pfxIsBeingImported = true
            return true;
        }

    }

    func extractURIParameter(_ url: String, name: String) -> String {
        let urlComponents = NSURLComponents(string: url)
        let from = urlComponents?.queryItems?.filter({ (el) in
            el.name == name
        }).first?.value
        return from!;
    }

}
