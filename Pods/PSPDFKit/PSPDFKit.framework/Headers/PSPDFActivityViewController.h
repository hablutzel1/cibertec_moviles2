//
//  PSPDFActivityViewController.h
//  PSPDFKit
//
//  Copyright © 2012-2016 PSPDFKit GmbH. All rights reserved.
//
//  THIS SOURCE CODE AND ANY ACCOMPANYING DOCUMENTATION ARE PROTECTED BY INTERNATIONAL COPYRIGHT LAW
//  AND MAY NOT BE RESOLD OR REDISTRIBUTED. USAGE IS BOUND TO THE PSPDFKIT LICENSE AGREEMENT.
//  UNAUTHORIZED REPRODUCTION OR DISTRIBUTION IS SUBJECT TO CIVIL AND CRIMINAL PENALTIES.
//  This notice may not be removed from this file.
//

#import "PSPDFEnvironment.h"

NS_ASSUME_NONNULL_BEGIN

/// Initialize with `PSPDFDocument` object as `activityItems` or directly with title/URL/NSData.
PSPDF_CLASS_AVAILABLE @interface PSPDFActivityViewController : UIActivityViewController @end

NS_ASSUME_NONNULL_END
