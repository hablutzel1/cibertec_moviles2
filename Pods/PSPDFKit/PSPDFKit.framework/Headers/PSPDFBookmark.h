//
//  PSPDFBookmark.h
//  PSPDFKit
//
//  Copyright © 2012-2016 PSPDFKit GmbH. All rights reserved.
//
//  THIS SOURCE CODE AND ANY ACCOMPANYING DOCUMENTATION ARE PROTECTED BY INTERNATIONAL COPYRIGHT LAW
//  AND MAY NOT BE RESOLD OR REDISTRIBUTED. USAGE IS BOUND TO THE PSPDFKIT LICENSE AGREEMENT.
//  UNAUTHORIZED REPRODUCTION OR DISTRIBUTION IS SUBJECT TO CIVIL AND CRIMINAL PENALTIES.
//  This notice may not be removed from this file.
//

#import "PSPDFModel.h"

NS_ASSUME_NONNULL_BEGIN

@class PSPDFAction;

/**
 A bookmark encapsulates a PDF action and a name. It is managed by a document's
 bookmark manager.
 
 @see PSPDFBookmarkManager
*/
PSPDF_CLASS_AVAILABLE @interface PSPDFBookmark : PSPDFModel

/// Initialize with an action object.
- (nullable instancetype)initWithAction:(PSPDFAction *)action;

/// The PDF action. Usually this will be of type `PSPDFGoToAction`, but all action types are possible.
@property (nonatomic, readonly) PSPDFAction *action;

/// A bookmark can have a name. This is optional.
@property (nonatomic, copy, nullable) NSString *name;

/**
 Contains the name of the bookmark or, if the bookmark does not have a valid name,
 a suitable description for the action of this bookmark.
 
 Typically this method is used to represent the bookmark in the UI.
 */
@property (nonatomic, readonly) NSString *displayName;

@end


@interface PSPDFBookmark (Deprecated)

/**
 Contains the name of the bookmark or, if the bookmark does not have a valid name,
 a suitable description for the action of this bookmark.

 @see displayName
 */
@property (nonatomic, readonly) NSString *pageOrNameString PSPDF_DEPRECATED("6.0", "Use displayName instead.");

@end

NS_ASSUME_NONNULL_END
