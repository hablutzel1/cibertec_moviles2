//
//  PSPDFRenderQueue.h
//  PSPDFKit
//
//  Copyright © 2012-2016 PSPDFKit GmbH. All rights reserved.
//
//  THIS SOURCE CODE AND ANY ACCOMPANYING DOCUMENTATION ARE PROTECTED BY INTERNATIONAL COPYRIGHT LAW
//  AND MAY NOT BE RESOLD OR REDISTRIBUTED. USAGE IS BOUND TO THE PSPDFKIT LICENSE AGREEMENT.
//  UNAUTHORIZED REPRODUCTION OR DISTRIBUTION IS SUBJECT TO CIVIL AND CRIMINAL PENALTIES.
//  This notice may not be removed from this file.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "PSPDFMacros.h"
#import "PSPDFAnnotation.h"

NS_ASSUME_NONNULL_BEGIN

@class PSPDFDocument, PSPDFRenderJob, PSPDFRenderQueue, PSPDFRenderReceipt, PSPDFRenderTask, PSPDFRenderRequest;

/// Absolute limit for image rendering (memory constraint)
PSPDF_EXPORT const CGSize PSPDFRenderSizeLimit;


typedef NS_ENUM(NSUInteger, PSPDFRenderQueuePriority) {
    /// Used for unspecified renderings with the lowest priority.
    PSPDFRenderQueuePriorityUnspecified = 0,

    /// Used for renderings that the user is not aware of, such as building a cache in the background.
    PSPDFRenderQueuePriorityBackground = 100,

    /// Used for renderings that the user might see but that are not necessary to complete, such as generating thumbnails that are not necessary for the user to properly work with a document but.
    PSPDFRenderQueuePriorityUtility = 200,

    /// Used for renderings that the user requested but that are not required for the user to keep using a document.
    PSPDFRenderQueuePriorityUserInitiated = 300,

    /// Used for renderings that the user requested and that are currently blocking their workflow.
    PSPDFRenderQueuePriorityUserInteractive = 400,


    /// Used to re-render annotation changes.
    PSPDFRenderQueuePriorityVeryLow PSPDF_DEPRECATED("6.0", "Use PSPDFRenderQueuePriorityUnspecified instead.") = PSPDFRenderQueuePriorityUnspecified,

    /// Low and VeryLow are used from within `PSPDFCache`.
    PSPDFRenderQueuePriorityLow PSPDF_DEPRECATED("6.0", "Use PSPDFRenderQueuePriorityBackground instead.") = PSPDFRenderQueuePriorityBackground,

    /// Live page renderings.
    PSPDFRenderQueuePriorityNormal PSPDF_DEPRECATED("6.0", "Use PSPDFRenderQueuePriorityUtility instead.") = PSPDFRenderQueuePriorityUtility,

    /// Zoomed renderings.
    PSPDFRenderQueuePriorityHigh PSPDF_DEPRECATED("6.0", "Use PSPDFRenderQueuePriorityUserInitiated instead.") = PSPDFRenderQueuePriorityUserInitiated,

    /// Highest priority. Unused.
    PSPDFRenderQueuePriorityVeryHigh PSPDF_DEPRECATED("6.0", "Use PSPDFRenderQueuePriorityUserInteractive instead.") = PSPDFRenderQueuePriorityUserInteractive,
} PSPDF_ENUM_AVAILABLE;


/// Render Queue. Does not cache. Used for rendering pages/page parts in `PSPDFPageView`.
PSPDF_CLASS_AVAILABLE_SUBCLASSING_RESTRICTED @interface PSPDFRenderQueue : NSObject

/// @name Requests

/**
 Schedules a render task in the receiving queue.
 
 The order in which tasks are executed is not necessarily the order in which they
 have been scheduled, nor the order of priority. The render queue makes an effort
 to serve as many tasks as possible in a timely manner. You should treat the order
 of execution of tasks as non-deterministic.

 @param task The render task to schedule in the queue.
 */
- (void)scheduleTask:(PSPDFRenderTask *)task NS_SWIFT_NAME(schedule(_:));

/// @name Settings

/// Amount of render tasks that run at the same time. Defaults to a value that is
/// best for the current device.
@property (atomic) NSUInteger concurrentRunningRenderRequests;

/// Cancel all queued and running jobs.
- (void)cancelAllJobs;

/// The minimum priority for tasks. Defaults to `PSPDFRenderQueuePriorityUnspecified`
/// which makes it run all tasks.
@property (atomic) PSPDFRenderQueuePriority minimumProcessPriority;

@end

NS_ASSUME_NONNULL_END
